package be.skcorps.hellokitten.repository.breed;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.hellokitten.model.Breeder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestBreedRepository {

    private BreedRepository repository = null;

    @BeforeAll
    public void beforeAllTests(){
        repository = new BreedRepository("JunitTests");
    }

    @Test
    void TestFindById(){
        Breed breed = repository.findByID(1);
        assertNotNull(breed); //er moet altijd een breed zijn met id 1 (eerste 19 zijn al aanwezig)
        assertEquals(Integer.valueOf(1),breed.getId()); //controleer of id effectief 1 is.
        assertNull(repository.findByID(-1)); //controleer of null wordt teruggeven als er geen breed is met die id
    }

    @Test
    void TestFindAll(){
        List<Breed> breedList = repository.findAll();
        assertNotNull(breedList);
        assertTrue(breedList.size()>=19);
    }

    @Test
    public void TestSaveAndDelete(){

        Breed breed = new Breed();
        breed.setName("Straatkat");
        breed.setPrice(0.0);
        breed.setStock(1000000);
        breed = repository.save(breed);
        //find saved breeder in DB
        Breed breedDB = repository.findByID(breed.getId());
        assertNotNull(breedDB);
        assertEquals("Straatkat",breedDB.getName());
        assertEquals(Double.valueOf(0.0), breedDB.getPrice());
        assertEquals(Integer.valueOf(1000000),breedDB.getStock());
        assertTrue(repository.delete(breedDB));
    }
}
