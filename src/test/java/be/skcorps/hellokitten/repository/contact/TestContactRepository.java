package be.skcorps.hellokitten.repository.contact;


import be.skcorps.hellokitten.model.Contact;
import org.junit.jupiter.api.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestContactRepository {

    private ContactRepository repository = null;

    @BeforeAll
    public void beforeAllTests(){
        repository = new ContactRepository("JunitTests");
    }

    @Test
    public void TestFindById(){
        Contact contact = repository.findByID(1);
        assertNotNull(contact);
        assertEquals(Integer.valueOf(1),contact.getId());
        assertNull(repository.findByID(0)); //id's zijn >=1, dus id 0 zit er niet in
    }

    @Test
    public void TestFindAll(){
        List<Contact> contacts = repository.findAll();
        assertNotNull(contacts);
    }

    @Test
    public void TestSaveAndDelete(){
        Contact newContact = new Contact("Kris","Coolen","Moorskampweg","1",
                "3680","Maaseik","kris.coolen@gmail.com","+32474433350");
        //save contact
        newContact = repository.save(newContact);
        //find saved contact in DB
        Contact contactDB = repository.findByID(newContact.getId());
        assertNotNull(contactDB);

        assertTrue(repository.delete(contactDB));
    }

    @Test
    public void TestUpdate(){
        Contact contact = repository.findByID(1);
        String oldFirstName = contact.getFirstName();
        String oldEmail = contact.getEmail();
        contact.setFirstName("Karolientje");
        contact.setEmail("Karolientje.desmet@skynet.be");
        assertTrue(repository.update(contact));
        Contact editedContact = repository.findByID(1);
        assertEquals("Karolientje",editedContact.getFirstName());
        assertEquals("Karolientje.desmet@skynet.be",editedContact.getEmail());
        //alles terug op oorspronkelijke zetten.
        contact.setFirstName(oldFirstName);
        contact.setEmail(oldEmail);
        assertTrue(repository.update(contact));
    }
}
