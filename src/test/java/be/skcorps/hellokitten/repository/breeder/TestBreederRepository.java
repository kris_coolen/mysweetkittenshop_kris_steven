package be.skcorps.hellokitten.repository.breeder;

import be.skcorps.hellokitten.model.Breeder;

import be.skcorps.hellokitten.model.Contact;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestBreederRepository {

    private BreederRepository repository = null;

    @BeforeAll
    public void beforeAllTests(){
        repository = new BreederRepository("JunitTests");
    }

    @Test
    public void TestFindById(){
        Breeder breeder = repository.findByID(1);
        assertNotNull(breeder);
        assertEquals(Integer.valueOf(1),breeder.getId());
        assertNull(repository.findByID(0)); //id's zijn >=1, dus id 0 zit er niet in
    }

    @Test
    public void TestFindAll(){
        List<Breeder> breeders = repository.findAll();
        assertNotNull(breeders);
    }

    @Test
    public void TestSaveAndDelete(){

        Breeder breeder = new Breeder();
        breeder.setName("De poesjes verwenner");
        breeder = repository.save(breeder);
        //find saved breeder in DB
        Breeder breederDB = repository.findByID(breeder.getId());
        assertNotNull(breederDB);
        assertEquals(breeder.getName(),breederDB.getName());
        assertTrue(repository.delete(breederDB));
    }


}


