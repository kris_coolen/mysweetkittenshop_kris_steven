<%@ page import="be.skcorps.hellokitten.model.Contact" %><%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 28/04/2020
  Time: 13:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Klant registratie resultaat</title>
    <%@ include file="/common/jspf/head.jspf"%>
</head>
<%
    Contact newContact = (Contact) request.getAttribute("ATTRIBUTE_NEWCONTACT");
%>
<body>
<main class="p-3">
    <%if (newContact!=null){%>
    De klant werd succesvol toegevoegd aan je klantenbestand. De gegevens van deze nieuwe klant zijn:
    <table class="table table-striped">
        <thead>
        <tr>
            <th>KLANTNR</th>
            <th>VOORNAAM</th>
            <th>ACHTERNAAM</th>
            <th>ADRES</th>
            <th>EMAIL</th>
            <th>TELEFOON</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><%=newContact.getId()%></td>
            <td><%=newContact.getFirstName()%></td>
            <td><%=newContact.getLastName()%></td>
            <td><%=newContact.getStreet()+" "+newContact.getNumber()+", "+newContact.getCity()%></td>
            <td><%=newContact.getEmail()%></td>
            <td><%=newContact.getTelephone()%></td>
        </tr>
        </tbody>
    </table>
    <% }else{ %>
    <a>Klant registratie mislukt.</a>
    <%} %>
</main>


</body>
</html>
