<%@ page import="be.skcorps.hellokitten.model.Breeder" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 28/04/2020
  Time: 23:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product toevoegen</title>
    <%@ include file="/common/jspf/head.jspf" %>
</head>
<%
List<Breeder> breederList =(List<Breeder>)request.getAttribute("ATTRIBUTE_BREEDERLIST");
    %>
<body>
<main class=p-3>
    <div class="align-content-center">
        Hieronder kan je kiezen van welke leverancier je een nieuw product wilt gaan verkopen. Bepaal ook de prijs en hoeveel
        kittens je zal aankopen van het ras naar keuze.
    </div>
    <div class="container-fluid">
        <form method="POST">

            <div class="form-group w-50">
                <label for="SelectSupplier">Selecteer de leverancier</label>
                <select class="form-control" name="PARAM_SUPPLIER" id="SelectSupplier">
                    <%for (Breeder breeder : breederList) {%>
                    <option><%=breeder.getId()+" "+breeder.getName()%>
                    </option>
                    <%}%>
                </select>
            </div>

            <div class="form-group w-50">
                <label for="inputBreedName">Bepaal naam kittenras: </label>
                <input class="form-control" type="text"  name="PARAM_BREEDNAME" id="inputBreedName">
            </div>

            <div class="form-group w-50">
                <label for="inputPrice">Bepaal prijs: </label>
                <input class="form-control" type="number" step=0.01 name="PARAM_PRICE" id="inputPrice">
            </div>

            <div class="form-group w-50">
                <label for="inputStock">Bepaal voorrraad: </label>
                <input class="form-control" type="number" name="PARAM_STOCK" id="inputStock">
            </div>
            <input class="btn-primary" type="submit" value="Voeg product toe"/><br>
        </form>
    </div>
</main>
</body>
</html>
