<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 28/04/2020
  Time: 16:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Resultaat aanpassen klantgegevens</title>
    <%@ include file="/common/jspf/head.jspf" %>
</head>
<%
    boolean success = (boolean) request.getAttribute("ATTRIBUTE_IS_UPDATED");
%>
<body>
<main class="p-3">
    <%if (success) {%>
    De klantgegevens zijn succesvol aangepast!
    <%} else {%>
    Oeps, De klantgegegevens werden niet aangepast.
    <%}%>
</main>

</body>
</html>
