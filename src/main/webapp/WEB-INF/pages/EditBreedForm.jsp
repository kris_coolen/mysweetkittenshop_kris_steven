<%@ page import="be.skcorps.hellokitten.model.Breed" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 28/04/2020
  Time: 22:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product aanpassen</title>
    <%@ include file="/common/jspf/head.jspf" %>
</head>
<%
    List<Breed> breedList =(List<Breed>)request.getAttribute("ATTRIBUTE_BREEDLIST");
%>
<body>
<main class=p-3>
    <div class="align-content-center">
        Hieronder kan je de prijs en/of voorraad wijzigen van de producten.
    </div>
    <div class="container-fluid">
        <form method="POST">
            <div class="form-group w-50">
                <label for="SelectProduct">Selecteer het product dat je wenst te wijzigen:</label>
                <select class="form-control" name="PARAM_PRODUCT" id="SelectProduct">
                    <%for (Breed breed : breedList) {%>
                    <option><%=breed.getId()+" "+breed.getName()+" van fokker " + breed.getBreeder().getName()%>
                    </option>
                    <%}%>
                </select>
            </div>
            <div class="form-group w-50">
                <label for="inputPrice">Wijzig prijs: </label>
                <input class="form-control" type="number" step=0.01 name="PARAM_EDIT_PRICE" id="inputPrice">

            <div class="form-group w-50">
                <label for="inputStock">Wijzig voorrraad: </label>
                <input class="form-control" type="number" name="PARAM_EDIT_STOCK" id="inputStock">
            </div>
            <input class="btn-primary" type="submit" value="Wijzig product"/><br>
        </form>
    </div>
</main>

</body>
</html>
