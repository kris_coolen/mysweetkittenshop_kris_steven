<%@ page import="be.skcorps.hellokitten.model.Breed" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 26/04/2020
  Time: 23:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Producten</title>
    <%@ include file="/common/jspf/head.jspf" %>
</head>
<body>
<%
    List<Breed> breeds = (List<Breed>) request.getAttribute("ATTRIBUTE_BREEDS");
%>

<%if (breeds != null) {%>
<main class="p-3">
    <h2>PRODUCTEN</h2>
    <table class="table table-striped w-75">
        <thead>
        <tr>
            <th>PRODUCTNR</th>
            <th>NAAM</th>
            <th>FOKKER</th>
            <th>PRIJS(€)</th>
            <th>STOCK</th>

        </tr>
        </thead>
        <tbody>
        <% for (Breed breed : breeds) {%>
        <tr>
            <td><%=breed.getId()%>
            </td>
            <td><%=breed.getName()%>
            </td>
            <td><%=breed.getBreeder().getName()%>
            </td>
            <td><%=breed.getPrice()%>
            </td>
            <td><%=breed.getStock()%>
            </td>

        </tr>
        <%
            }
            ;
        %>
        </tbody>
    </table>
    <% } else { %>
    <a>Oops, er werden geen producten gevonden.</a>
    <%} %>
</main>
</body>
</html>
