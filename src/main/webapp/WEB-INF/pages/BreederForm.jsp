<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 28/04/2020
  Time: 23:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Voeg leverancier toe</title>
    <%@ include file="/common/jspf/head.jspf" %>
</head>
<body>

<main class="p-3">
    <div class="container-fluid">
        <h1 class="align-content-center">Leveranciersgegevens</h1>
        <form class="was-validated" method="POST">
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputSupplierName">Bedrijfsnaam: </label>
                        <input type="text" name="PARAM_SUPPLIER_NAME" class="form-control" id="inputSupplierName"
                               placeholder="Vul bedrijfsnaam in" required>
                        <div class="valid-feedback">Geldig</div>
                        <div class="invalid-feedback">Bedrijfsnaam verplicht</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputName">Achternaam contactpersoon: </label>
                        <input type="text" name="PARAM_SUPPLIER_LASTNAME" class="form-control" id="inputName"
                               placeholder="Vul achternaam in" required>
                        <div class="valid-feedback">Geldig</div>
                        <div class="invalid-feedback">Achternaam verplicht</div>
                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputFirstName">Voornaam contactpersoon: </label>
                        <input type="text" name="PARAM_SUPPLIER_FIRSTNAME" class="form-control" id="inputFirstName"
                               placeholder="Vul voornaam in" required>
                        <div class="valid-feedback">Geldig</div>
                        <div class="invalid-feedback">Voornaam verplicht</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputStreet">Straat leverancier: </label>
                        <input type="text" name="PARAM_SUPPLIER_STREET" class="form-control" id="inputStreet"
                               placeholder="Vul straat in" required>
                        <div class="valid-feedback">Geldig</div>
                        <div class="invalid-feedback">Straat verplicht</div>
                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputNumber">Nummer </label>
                        <input type="text" name="PARAM_SUPPLIER_NUMBER" class="form-control" id="inputNumber"
                               placeholder="Vul nummer in" required>
                        <div class="valid-feedback">Geldig</div>
                        <div class="invalid-feedback">Nummer verplicht</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputZipCode">Postcode </label>
                        <input type="text" name="PARAM_SUPPLIER_ZIPCODE" class="form-control" id="inputZipCode"
                               placeholder="Vul postcode in">

                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputCity">Gemeente </label>
                        <input type="text" name="PARAM_SUPPLIER_CITY" class="form-control" id="inputCity"
                               placeholder="Vul gemeente in" required>
                        <div class="valid-feedback">Geldig</div>
                        <div class="invalid-feedback">Gemeente verplicht</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <div class="form-group">
                        <label for="inputEmail">Email </label>
                        <input type="email" name="PARAM_SUPPLIER_EMAIL" class="form-control" id="inputEmail"
                               placeholder="Vul email in" required>
                        <div class="valid-feedback">Geldig email adres</div>
                        <div class="invalid-feedback">Vul een geldig email adres in</div>
                    </div>
                </div>
                <div class="column">
                    <div class="form-group">
                        <label for="inputTelephoneNumber">Telephone number </label>
                        <input type="tel" name="PARAM_SUPPLIER_TELEPHONE" class="form-control" id="inputTelephoneNumber"
                               placeholder="Enter your telephone number" pattern="\+324[0-9]{8}" required>
                        <div class="valid-feedback">Geldig</div>
                        <div class="invalid-feedback">Vul een geldig telefoonnummer in</div>
                        <small>Format: +324xxxxxxxx met x een getal tussen 0 en 9</small>
                    </div>
                </div>
            </div>
            <input type="submit" value="Voeg leverancier toe" class="btn-primary"/><br>
        </form>
    </div>
</main>

</body>
</html>
