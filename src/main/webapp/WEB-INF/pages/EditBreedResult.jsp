<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 28/04/2020
  Time: 22:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%@ include file="/common/jspf/head.jspf" %>
</head>
<%
    boolean success = (boolean) request.getAttribute("ATTRIBUTE_BREED_IS_UPDATED");
%>
<body>
<main class="p-3">
    <%if (success) {%>
    De prijs en/of voorraad van het product zijn succesvol aangepast!
    <%} else {%>
    Oeps, De productgegevens werden niet aangepast.
    <%}%>
</main>

</body>
</html>
