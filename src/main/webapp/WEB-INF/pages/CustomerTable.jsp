<%@ page import="be.skcorps.hellokitten.model.Contact" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 26/04/2020
  Time: 10:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Klanten</title>
    <%@ include file="/common/jspf/head.jspf"%>
</head>
<body>
<%
    List<Contact> contacts =(List<Contact>)request.getAttribute("ATTRIBUTE_CONTACTS");
%>
<main class="p-3">
    <%if (contacts!=null){%>
    <h2>KLANTENBESTAND</h2>
    <table class="table table-striped w-75" >
        <thead>
        <tr>
            <th>KLANTNR</th>
            <th>VOORNAAM</th>
            <th>ACHTERNAAM</th>
            <th>ADRES</th>
            <th>EMAIL</th>
            <th>TELEFOON</th>
        </tr>
        </thead>
        <tbody>
        <% for(Contact c: contacts) {%>
        <tr>
            <td><%=c.getId()%></td>
            <td><%=c.getFirstName()%></td>
            <td><%=c.getLastName()%></td>
            <td><%=c.getStreet()+" "+c.getNumber()+", "+c.getCity()%></td>
            <td><%=c.getEmail()%></td>
            <td><%=c.getTelephone()%></td>
        </tr>
        <%
            };
        %>
        </tbody>
    </table>
    <% }else{ %>
    <a>Oops, er werden geen klanten gevonden.</a>
    <%} %>
</main>

</body>
</html>
