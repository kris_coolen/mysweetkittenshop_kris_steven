<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 28/04/2020
  Time: 14:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Aanpassen klantgegevens</title>
    <%@ include file="/common/jspf/head.jspf" %>
</head>
<%
    List<Integer> idList = (List<Integer>) request.getAttribute("ATTRIBUTE_CONTACT_ID_LIST");
%>
<body>
<main class="p3">
    <div class="align-content-center">
        Vul onderstaand formulier in om een klant te wijzigen.
        Indien je bepaalde velden niet wil wijzigen, hoef je dit veld niet in te vullen!
    </div>
    <div class="container-fluid">
        <form method="POST">
            <div class="form-group w-50">
                <label for="SelectContactId">Selecteer het klantennummer van de klant die je wenst te wijzigen</label>
                <select class="form-control" name="PARAM_CONTACT_ID" id="SelectContactId">
                    <%for (Integer id : idList) {%>
                    <option><%=id%>
                    </option>
                    <%}%>
                </select>
            </div>
            <div class="form-group w-50">
                <label for="inputName">Wijzig achternaam </label>
                <input class="form-control" type="text" name="PARAM_EDIT_LASTNAME" id="inputName">

            </div>
            <div class="form-group w-50">
                <label for="inputFirstName">Wijzig voornaam </label>
                <input class="form-control" type="text" name="PARAM_EDIT_FIRSTNAME" id="inputFirstName">
            </div>

            <div class="form-group w-50">
                <label for="inputStreet">Wijzig straat </label>
                <input class="form-control" type="text" name="PARAM_EDIT_STREET" id="inputStreet">
            </div>
            <div class="form-group w-50">
                <label for="inputNumber">Wijzig nummer</label>
                <input class="form-control" type="text" name="PARAM_EDIT_NUMBER" id="inputNumber">
            </div>
            <div class="form-group w-50">
                <label for="inputZipCode">Wijzig postcode</label>
                <input class="form-control" type="text" name="PARAM_EDIT_ZIPCODE" id="inputZipCode">
            </div>
            <div class="form-group w-50">
                <label for="inputCity">Wijzig gemeente</label>
                <input class="form-control" type="text" name="PARAM_EDIT_CITY" id="inputCity">
            </div>
            <div class="form-group w-50">
                <label for="inputEmail">Wijzig email</label>
                <input class="form-control" type="email" name="PARAM_EDIT_EMAIL" id="inputEmail">
            </div>
            <div class="form-group w-50">
                <label for="inputTel">Wijzig telefoonnummer</label>
                <input class="form-control" type="tel" name="PARAM_EDIT_TEL" id="inputTel">
            </div>
            <input class="btn-primary" type="submit" value="Wijzig klant"/><br>
        </form>
    </div>
</main>

</body>
</html>