<%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 28/04/2020
  Time: 23:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Leverancier registratie resultaat</title>
    <%@ include file="/common/jspf/head.jspf"%>
</head>
<body>
<main class="p-5">
    Leverancier succesvol aangemaakt. Contactgegevens van de leverancier vind je terug onder het klantenbestand.
</main>
</body>
</html>
