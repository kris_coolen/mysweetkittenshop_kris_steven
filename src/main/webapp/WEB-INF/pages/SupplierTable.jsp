<%@ page import="be.skcorps.hellokitten.model.Breeder" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Java01
  Date: 26/04/2020
  Time: 22:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Leveranciers</title>
    <%@ include file="/common/jspf/head.jspf" %>
</head>
<body>
<%
    List<Breeder> breeders = (List<Breeder>) request.getAttribute("ATTRIBUTE_BREEDERS");
%>

<%if (breeders != null) {%>
<main class="p-3">
    <h2>LEVERANCIERS</h2>
    <table class="table table-striped w-75">
        <thead>
        <tr>
            <th>FOKKERNR</th>
            <th>BEDRIJFSNAAM</th>
            <th>CONTACT</th>
            <th>RASSEN</th>
        </tr>
        </thead>
        <tbody>
        <% for (Breeder breeder : breeders) {%>
        <tr>
            <td><%=breeder.getId()%>
            </td>
            <td><%=breeder.getName()%>
            </td>
            <td><%=breeder.getContact().getFirstName() + " " + breeder.getContact().getLastName()%>
            </td>
            <td><%=breeder.getBreeds().toString()%>
            </td>
        </tr>
        <%
            }
            ;
        %>
        </tbody>
    </table>
    <% } else { %>
    <a>Oops, er werden geen leveranciers gevonden.</a>
    <%} %>
</main>
</body>
</html>
