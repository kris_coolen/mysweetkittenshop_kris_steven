CREATE DATABASE  IF NOT EXISTS `javaeegenkDB1` /*!40100 DEFAULT CHARACTER SET latin1 ;
USE `javaeegenkDB1`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: www.noelvaes.eu    Database: javaeegenkDB1
-- ------------------------------------------------------
-- Server version	5.5.65-MariaDB

SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT;
SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS;
SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION;
SET NAMES utf8;
SET @OLD_TIME_ZONE=@@TIME_ZONE;
SET TIME_ZONE='+00:00';
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';
SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0;

--
-- Dumping data for table `BREEDERS`
--

LOCK TABLES `BREEDERS` WRITE;
ALTER TABLE `BREEDERS` DISABLE KEYS;
INSERT INTO `BREEDERS` VALUES (1,'Black Mystic Legend',8),(2,'Sequoia Hoeve',9),(3,'Ankeshof',10),(4,'Lieve Poezies',11),(5,'Katia\'s Palazo',12),(6,'Kitty Wizzy',13),(7,'Groot Kathof',14);
ALTER TABLE `BREEDERS` ENABLE KEYS;
UNLOCK TABLES;

--
-- Dumping data for table `BREEDS`
--

LOCK TABLES `BREEDS` WRITE;
ALTER TABLE `BREEDS` DISABLE KEYS;
INSERT INTO `BREEDS` VALUES (1,'Heilige Birmaan',1400,1,1),(2,'Heilige Birmaan',1245,5,7),(3,'Britse Korthaar',900,2,2),(4,'Britse Korthaar',750,3,7),(5,'Europese Korthaar',500,3,2),(6,'Europese Korthaar',400,4,5),(7,'Europese Korthaar',350,5,7),(8,'Naakt',1100,4,3),(9,'Naakt',1000,5,6),(10,'Naakt',900,6,7),(11,'Main Coon',1000,2,4),(12,'Main Coon',950,3,7),(13,'Perzische',475,4,4),(14,'Perzische',450,6,7),(15,'Noorse Bos',685,7,5),(16,'Noorse Bos',625,3,7),(17,'Siamees',850,1,6),(18,'Siamees',650,4,7),(19,'Bengaal',1200,3,7);
ALTER TABLE `BREEDS` ENABLE KEYS;
UNLOCK TABLES;

--
-- Dumping data for table `CONTACTS`
--

LOCK TABLES `CONTACTS` WRITE;
ALTER TABLE `CONTACTS` DISABLE KEYS;
INSERT INTO `CONTACTS` VALUES (1,'Oostende','Karolien.de.smet@gmail.com','Karolien','De Smet','22','FraiseLaan','+32473645399',NULL),
                              (2,'Aalter','Freya.poessie@hotmail.com','Freya','Lievens','10','Van Loo Straat','+32492332233',NULL),
                              (3,'Gent','karelvagrote@hotmail.com','Karel','de Grote','1A','Burggrachtdreef','+32487756473',NULL),
                              (4,'Merelbeke','Suzzy.wuzzy@me.com','Suzzy','Lalijo','174','Vleeshuisstraat','+32499934843',NULL),
                              (5,'Gent','Levi@multimedi.be','Levi','Helders','42','Vlindertuinstraat','+32491234563',NULL),
                              (6,'Eeklo','fleur@msn.be','Fleur','Potters','500','Chrysanten straat','+32472323645',NULL),
                              (7,'Merelbeke','kellieke@versegroenten.be','Kelly','Verlinde','900B','Gentseweg','+32610283736',NULL),
                              (8,'Gent','info@blackmystic.be','Sonja','Verliefde','13','Cupidolaan','+32475101044',NULL),
                              (9,'Leiden','info@sequoaiahoeve.nl','Sandra','Gezelle','7','Franksestraat','+32688347273','2345AB'),
                              (10,'Merelbeke','info@ankeshof.be','Anke','Van Hoven','94','Hofseweg','+32484328432',NULL),
                              (11,'Gent','Lieve.poezies@gmail.com','Lieve','De Schat','65','Kasseiweg','+32490101010',NULL),
                              (12,'Antwerpen','info@katias.palazo.be','Katia','Ligatino','233','Heirbaan','+32483618361',NULL),
                              (13,'Vleeteren','Wizzy.info@kitties.be','Wiese','Berlind','23B','Vleetseweg','+32491231239',NULL),
                              (14,'Oostende','info@groot-kathof.be','Catharina','Bruyntjes','90','Mechelsevest','+32472793542',NULL);
ALTER TABLE `CONTACTS` ENABLE KEYS;
UNLOCK TABLES;
SET TIME_ZONE=@OLD_TIME_ZONE;

SET SQL_MODE=@OLD_SQL_MODE ;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS ;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS ;
SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT ;
SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS ;
SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION ;
SET SQL_NOTES=@OLD_SQL_NOTES ;

-- Dump completed on 2020-04-28 11:24:54
