create table if not exists javaeegenkDB3.CONTACTS
(
    ID         int auto_increment
        primary key,
    CITY       varchar(255) null,
    EMAIL      varchar(255) null,
    FIRST_NAME varchar(255) null,
    LAST_NAME  varchar(255) null,
    NUMBER     varchar(255) null,
    STREET     varchar(255) null,
    TELEPHONE  varchar(255) null,
    ZIPCODE    varchar(255) null
);

create table if not exists javaeegenkDB3.BREEDERS
(
    ID         int auto_increment
        primary key,
    NAME       varchar(255) null,
    contact_ID int          null,
    constraint FK3dikku9l03hpyee9w12y7yh3j
        foreign key (contact_ID) references javaeegenkDB3.CONTACTS (ID)
);

create table if not exists javaeegenkDB3.BREEDS
(
    ID         int auto_increment
        primary key,
    NAME       varchar(255) null,
    PRICE      double       null,
    STOCK      int          null,
    breeder_ID int          null,
    constraint FKe839jrgku0qigueuhcugg8t0o
        foreign key (breeder_ID) references javaeegenkDB3.BREEDERS (ID)
);


