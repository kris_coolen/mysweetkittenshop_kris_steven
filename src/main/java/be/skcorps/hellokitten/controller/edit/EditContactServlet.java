package be.skcorps.hellokitten.controller.edit;

import be.skcorps.hellokitten.model.Contact;
import be.skcorps.hellokitten.repository.contact.ContactRepository;
import be.skcorps.hellokitten.service.ContactService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/editclient")
public class EditContactServlet extends HttpServlet {

    ContactService service = null;

    @Override
    public void init(){
        service = new ContactService(new ContactRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Contact> contactList = service.findAllContacts();
        List<Integer> idList = new ArrayList<>();
        for(Contact c: contactList){
            idList.add(c.getId());
        }
        req.setAttribute("ATTRIBUTE_CONTACT_ID_LIST",idList);
        req.getRequestDispatcher("WEB-INF/pages/EditContactForm.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String IdString = req.getParameter("PARAM_CONTACT_ID");
        Contact contact = service.findContactById(Integer.valueOf(IdString));

        String lastName = req.getParameter("PARAM_EDIT_LASTNAME");
        if(lastName!=null && !lastName.isBlank()){
            contact.setLastName(lastName);
        }
        String firstName = req.getParameter("PARAM_EDIT_FIRSTNAME");
        if(firstName!=null && !firstName.isBlank()){
            contact.setFirstName(firstName);
        }
        String street = req.getParameter("PARAM_EDIT_STREET");
        if(street!=null && !street.isBlank()){
            contact.setStreet(street);
        }

        String number = req.getParameter("PARAM_EDIT_NUMBER");
        if(number!=null && !number.isBlank()){
            contact.setNumber(number);
        }

        String zipcode = req.getParameter("PARAM_EDIT_ZIPCODE");
        if(zipcode!=null && !zipcode.isBlank()){
            contact.setZipCode(zipcode);
        }

        String city = req.getParameter("PARAM_EDIT_CITY");
        if(city!=null && !city.isBlank()){
            contact.setCity(city);
        }

        String email = req.getParameter("PARAM_EDIT_EMAIL");
        if(email!=null && !email.isBlank()){
            contact.setEmail(email);
        }

        String tel = req.getParameter("PARAM_EDIT_TELEPHONE");
        if(tel!=null && !tel.isBlank()){
            contact.setTelephone(tel);
        }

        boolean success = service.updateContact(contact);
        req.setAttribute("ATTRIBUTE_IS_UPDATED",success);
        req.getRequestDispatcher("WEB-INF/pages/EditContactResult.jsp").forward(req,resp);

    }
}
