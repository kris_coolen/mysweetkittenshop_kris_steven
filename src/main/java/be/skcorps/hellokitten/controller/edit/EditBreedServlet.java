package be.skcorps.hellokitten.controller.edit;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.hellokitten.model.Contact;
import be.skcorps.hellokitten.repository.breed.BreedRepository;
import be.skcorps.hellokitten.service.BreedService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/editproduct")
public class EditBreedServlet extends HttpServlet {

    BreedService service = null;

    @Override
    public void init() {
       service = new BreedService(new BreedRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Breed> breedList = service.findAllBreeds();

        req.setAttribute("ATTRIBUTE_BREEDLIST",breedList);
        req.getRequestDispatcher("WEB-INF/pages/EditBreedForm.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String paramProduct = req.getParameter("PARAM_PRODUCT");
        String idString = paramProduct.substring(0,1);
        Breed breed = service.findBreedById(Integer.valueOf(idString));

        String priceString = req.getParameter("PARAM_EDIT_PRICE");
        Double price = Double.valueOf(priceString);
        if(priceString!=null && !priceString.isBlank()){
            breed.setPrice(price);
        }
        String stockString = req.getParameter("PARAM_EDIT_STOCK");
        Integer stock = Integer.valueOf(stockString);
        if(stockString!=null && !stockString.isBlank()){
            breed.setStock(stock);
        }

        boolean success = service.updateBreed(breed);
        req.setAttribute("ATTRIBUTE_BREED_IS_UPDATED",success);
        req.getRequestDispatcher("WEB-INF/pages/EditBreedResult.jsp").forward(req,resp);
    }
}
