package be.skcorps.hellokitten.controller.show;

import be.skcorps.hellokitten.model.Contact;
import be.skcorps.hellokitten.repository.contact.ContactRepository;
import be.skcorps.hellokitten.service.ContactService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/showclients")
public class ShowContactsServlet extends HttpServlet {

    ContactService service = null;

    @Override
    public void init() {
        service = new ContactService(new ContactRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Contact> contacts = service.findAllContacts();
        req.setAttribute("ATTRIBUTE_CONTACTS",contacts);
        req.getRequestDispatcher("/WEB-INF/pages/CustomerTable.jsp").forward(req,resp);

    }


}
