package be.skcorps.hellokitten.controller.show;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.hellokitten.repository.breed.BreedRepository;
import be.skcorps.hellokitten.service.BreedService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/showproducts")
public class ShowBreedsServlet extends HttpServlet {

    private BreedService service;

    @Override
    public void init(){
        service = new BreedService(new BreedRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Breed> breeds = service.findAllBreeds();
        req.setAttribute("ATTRIBUTE_BREEDS",breeds);
        req.getRequestDispatcher("/WEB-INF/pages/ProductTable.jsp").forward(req,resp);
    }
}
