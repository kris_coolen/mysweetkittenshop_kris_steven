package be.skcorps.hellokitten.controller.show;

import be.skcorps.hellokitten.model.Breeder;
import be.skcorps.hellokitten.model.Contact;
import be.skcorps.hellokitten.repository.breeder.BreederRepository;
import be.skcorps.hellokitten.service.BreederService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/showsuppliers")
public class ShowBreedersServlet extends HttpServlet {

    BreederService service = null;

    @Override
    public void init()  {
        service = new BreederService(new BreederRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Breeder> breeders = service.findAllBreeders();
        req.setAttribute("ATTRIBUTE_BREEDERS",breeders);
        req.getRequestDispatcher("/WEB-INF/pages/SupplierTable.jsp").forward(req,resp);

    }
}
