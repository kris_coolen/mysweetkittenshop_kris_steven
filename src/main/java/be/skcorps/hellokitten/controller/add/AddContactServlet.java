package be.skcorps.hellokitten.controller.add;

import be.skcorps.hellokitten.model.Contact;
import be.skcorps.hellokitten.repository.contact.ContactRepository;
import be.skcorps.hellokitten.service.ContactService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addclient")
public class AddContactServlet extends HttpServlet {

    ContactService service = null;

    @Override
    public void init() throws ServletException {
        service = new ContactService(new ContactRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/pages/ContactForm.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firstName = req.getParameter("PARAM_FIRSTNAME");
        String lastName = req.getParameter("PARAM_LASTNAME");
        String street = req.getParameter("PARAM_STREET");
        String number = req.getParameter("PARAM_NUMBER");
        String zipCode = req.getParameter("PARAM_ZIPCODE");
        String city = req.getParameter("PARAM_CITY");
        String email = req.getParameter("PARAM_EMAIL");
        String telephone = req.getParameter("PARAM_TELEPHONE");
        Contact contact = new Contact(firstName,lastName,street,number,zipCode,city,email,telephone);
        contact = service.saveContact(contact);
        req.setAttribute("ATTRIBUTE_NEWCONTACT",contact);
        req.getRequestDispatcher("WEB-INF/pages/ContactResult.jsp").forward(req,resp);


    }
}
