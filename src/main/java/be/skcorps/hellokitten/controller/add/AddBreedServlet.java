package be.skcorps.hellokitten.controller.add;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.hellokitten.model.Breeder;
import be.skcorps.hellokitten.repository.breed.BreedRepository;
import be.skcorps.hellokitten.repository.breeder.BreederRepository;
import be.skcorps.hellokitten.service.BreedService;
import be.skcorps.hellokitten.service.BreederService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/addproduct")
public class AddBreedServlet extends HttpServlet {

    private BreedService breedService= null;
    private BreederService breederService = null;

    @Override
    public void init() {
        breedService = new BreedService(new BreedRepository());
        breederService = new BreederService(new BreederRepository());

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Breeder> breederList = breederService.findAllBreeders();
        req.setAttribute("ATTRIBUTE_BREEDERLIST",breederList);
        req.getRequestDispatcher("WEB-INF/pages/BreedForm.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String paramBreeder = req.getParameter("PARAM_SUPPLIER");
        String idString = paramBreeder.substring(0,1);
        Breeder breeder = breederService.findBreederById(Integer.valueOf(idString));


        String priceString = req.getParameter("PARAM_PRICE");
        Double price = Double.valueOf(priceString);

        String stockString = req.getParameter("PARAM_STOCK");
        Integer stock = Integer.valueOf(stockString);

        String breedName = req.getParameter("PARAM_BREEDNAME");

        Breed breed = new Breed(breedName,price,stock);
        breed.setBreeder(breeder);

        breed = breedService.saveBreed(breed);
        req.setAttribute("ATTRIBUTE_NEWBREED",breed);
        req.getRequestDispatcher("WEB-INF/pages/BreedResult.jsp").forward(req,resp);
    }
}
