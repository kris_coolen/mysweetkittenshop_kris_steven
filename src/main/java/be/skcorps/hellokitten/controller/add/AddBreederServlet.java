package be.skcorps.hellokitten.controller.add;

import be.skcorps.hellokitten.model.Breeder;
import be.skcorps.hellokitten.model.Contact;
import be.skcorps.hellokitten.repository.breeder.BreederRepository;
import be.skcorps.hellokitten.service.BreederService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addsupplier")
public class AddBreederServlet extends HttpServlet {
    BreederService service=null;

    @Override
    public void init()  {
        service = new BreederService(new BreederRepository());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/pages/BreederForm.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("PARAM_SUPPLIER_NAME");
        String firstName = req.getParameter("PARAM_SUPPLIER_FIRSTNAME");
        String lastName = req.getParameter("PARAM_SUPPLIER_LASTNAME");
        String street = req.getParameter("PARAM_SUPPLIER_STREET");
        String number = req.getParameter("PARAM_SUPPLIER_NUMBER");
        String zipCode = req.getParameter("PARAM_SUPPLIER_ZIPCODE");
        String city = req.getParameter("PARAM_SUPPLIER_CITY");
        String email = req.getParameter("PARAM_SUPPLIER_EMAIL");
        String telephone = req.getParameter("PARAM_SUPPLIER_TELEPHONE");
        Contact contact = new Contact(firstName,lastName,street,number,zipCode,city,email,telephone);
        Breeder breeder = new Breeder(name,contact);
        breeder = service.saveBreeder(breeder);
        req.setAttribute("ATTRIBUTE_NEWBREEDER",breeder);
        req.getRequestDispatcher("WEB-INF/pages/BreederResult.jsp").forward(req,resp);
    }
}
