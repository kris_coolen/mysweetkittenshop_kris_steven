package be.skcorps.hellokitten;

import be.skcorps.hellokitten.model.*;
import be.skcorps.hellokitten.repository.InitialDataBase;

import javax.persistence.*;

import static be.skcorps.hellokitten.repository.InitialDataBase.*;


public class PopulateDataBase {

    public static void populate(String persistenceUnit){
        EntityManagerFactory emf = null;
        EntityManager em = null;
        try {
            emf = Persistence.createEntityManagerFactory(persistenceUnit);
            em = emf.createEntityManager();
            EntityTransaction tx = em.getTransaction();

            //fill table CONTACTS
            for(int i=0; i<firstNames.length;i++) {
                Contact contact = new Contact(firstNames[i], lastNames[i], streets[i], numbers[i], zipCodes[i],
                        cities[i], emails[i], telephones[i]);
                tx.begin();
                em.persist(contact);
                tx.commit();
            }
            //fill table BREEDS (except for BREEDID FK)
            for(Breed b: breedList) {
                tx.begin();
                em.persist(b);
                tx.commit();
            }

            //fill table BREEDERS and link with BREEDS table
            for(int i=0; i<breederNames.size();i++){
                Breeder breeder = new Breeder();
                breeder.setName(breederNames.get(i));
                Contact con = em.find(Contact.class,contactIdList.get(i));
                breeder.setContact(con);
                for(Integer breedId:breedIdsOf.get(i)){
                    Breed breed = em.find(Breed.class,breedId);
                    breeder.addBreed(breed);
                }
                tx.begin();
                em.persist(breeder);
                tx.commit();

                //create empty order
                Order order = new Order();
                tx.begin();
                em.persist(order);
                tx.commit();
            }
        } finally {
            if(em!=null) em.close();
            if(emf!=null) emf.close();
        }


    }

    public static void main(String[] args) {
        InitialDataBase.init();
        populate("webshop");
    }

}
