package be.skcorps.hellokitten.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="CONTACTS")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Integer id;


    @Column(name="FIRST_NAME")
    private String firstName;

    @Column(name="LAST_NAME")
    private String lastName;

    @Column(name="STREET")
    private String street;

    @Column(name="NUMBER")
    private String number;

    @Column(name="ZIPCODE")
    private String zipCode;

    @Column(name="CITY")
    private String city;

    @Column(name="EMAIL")
    private String email;

    @Column(name="TELEPHONE")
    private String telephone;

    public Contact(String firstName, String lastName, String street, String number, String zipCode, String city, String email, String telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.street = street;
        this.number = number;
        this.zipCode = zipCode;
        this.city = city;
        this.email = email;
        this.telephone = telephone;
    }

    public Contact(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Contact)) return false;
        Contact contact = (Contact) o;
        return Objects.equals(getFirstName(), contact.getFirstName()) &&
                Objects.equals(getLastName(), contact.getLastName()) &&
                Objects.equals(getStreet(), contact.getStreet()) &&
                Objects.equals(getNumber(), contact.getNumber()) &&
                Objects.equals(getZipCode(), contact.getZipCode()) &&
                Objects.equals(getCity(), contact.getCity()) &&
                Objects.equals(getEmail(), contact.getEmail()) &&
                Objects.equals(getTelephone(), contact.getTelephone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getStreet(), getNumber(), getZipCode(),
                getCity(), getEmail(), getTelephone());
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                '}';
    }
}
