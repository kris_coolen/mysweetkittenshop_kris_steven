package be.skcorps.hellokitten.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="BREEDS")
public class Breed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Integer id;

    @Column(name="NAME")
    private String name;

    @Column(name="PRICE")
    private Double price;

    @Column(name="STOCK")
    private Integer stock;

    @ManyToOne
    private Breeder breeder;

    public Breed() {
    }

    public Breed(String name, Double price, Integer stock){
        this.name=name;
        this.price=price;
        this.stock=stock;
    }

    public Integer getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Breeder getBreeder() {
        return breeder;
    }

    public void setBreeder(Breeder breeder) {
        this.breeder = breeder;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Breed)) return false;
        Breed breed = (Breed) o;
        return Objects.equals(getName(), breed.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
