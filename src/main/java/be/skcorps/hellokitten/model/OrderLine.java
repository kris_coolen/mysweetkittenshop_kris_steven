package be.skcorps.hellokitten.model;

import javax.persistence.*;

@Entity
@Table(name="ORDERLINES")
public class OrderLine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Integer id;

    @Column(name="QUANTITY")
    private Integer quantity;

    @ManyToOne
    private Order order;

    @OneToOne
    private Breed breed;

    public Integer getId() {
        return id;
    }

    public OrderLine(Integer quantity, Order order, Breed breed) {
        this.quantity = quantity;
        this.order = order;
        this.breed = breed;
    }

    public OrderLine(){

    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }
}
