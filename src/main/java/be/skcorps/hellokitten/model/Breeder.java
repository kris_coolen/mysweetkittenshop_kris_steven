package be.skcorps.hellokitten.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="BREEDERS")
public class Breeder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private Integer id;

    @Column(name="NAME")
    private String name;

    @OneToOne(cascade = CascadeType.ALL)
    private Contact contact;

    @OneToMany(mappedBy = "breeder",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    Set<Breed> breeds = new HashSet<>();

    public Breeder() {
    }

    public Breeder(String name,Contact contact){
        this.name=name;
        this.contact=contact;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Set<Breed> getBreeds() {
        return breeds;
    }

    public void setBreeds(Set<Breed> breeds) {
        this.breeds = breeds;
    }

    public void addBreed(Breed breed){
        breeds.add(breed);
        breed.setBreeder(this);
    }

    public void removeBreed(Breed breed){
        breeds.remove(breed);
        breed.setBreeder(null);
    }




}
