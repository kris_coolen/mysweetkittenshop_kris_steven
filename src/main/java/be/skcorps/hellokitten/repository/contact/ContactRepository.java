package be.skcorps.hellokitten.repository.contact;

import be.skcorps.hellokitten.model.Contact;
import be.skcorps.persistence.PersistenceManager;

import javax.persistence.*;
import java.util.List;

public class ContactRepository implements ContactDAO {

    private EntityManagerFactory emf = null;

    public ContactRepository(String persistenceUnit){
        emf = PersistenceManager.getInstance().getFactory(persistenceUnit);

    }

    public ContactRepository(){
        this("webshop");
    }

    @Override
    public List<Contact> findAll() {
        EntityManager em = null;
        List<Contact> contacts = null;
        try{
            em = emf.createEntityManager();
            TypedQuery<Contact> query = em.createNamedQuery("Contact.findAllContacts",Contact.class);
            contacts = query.getResultList();
        }catch (Exception exception){
            exception.printStackTrace();

        } finally {
            if (em != null)
                em.close();

        }
        return contacts;
    }

    @Override
    public Contact findByID(Integer id) {
        EntityManager em = null;
        Contact contact = null;
        try{
            em = emf.createEntityManager();
            TypedQuery<Contact> query =
                    em.createQuery("select contact from Contact as contact where contact.id="+id,Contact.class);

            contact = query.getSingleResult();

        }catch(Exception exception){
            exception.printStackTrace();
        }
        finally{
            if(em!=null) em.close();
        }
        return contact;
    }

    @Override
    public Contact save(Contact contact) {
        EntityManager em=null;
        EntityTransaction tx = null;
        try{
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(contact);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return contact;
    }

    @Override
    public boolean delete(Contact contact) {
        EntityManager em=null;
        EntityTransaction tx = null;
        boolean isDeleted = false;
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Contact contactDB = em.find(Contact.class,contact.getId());
            tx.commit();
            if (contactDB != null) {
                tx.begin();
                    em.remove(contactDB);
                tx.commit();
                isDeleted = true;
            }
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return isDeleted;
    }

    @Override
    public boolean update(Contact contact) {

        EntityManager em = null;
        EntityTransaction tx = null;
        boolean isUpdated =false;
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Contact contactDB = em.find(Contact.class, contact.getId());
            tx.commit();
            if (contactDB != null){
                tx.begin();
                contactDB.setFirstName(contact.getFirstName());
                contactDB.setLastName(contact.getLastName());
                contactDB.setStreet(contact.getStreet());
                contactDB.setNumber(contact.getNumber());
                contactDB.setZipCode(contact.getZipCode());
                contactDB.setCity(contact.getCity());
                contactDB.setEmail(contact.getEmail());
                contactDB.setTelephone(contact.getTelephone());
                tx.commit();
                isUpdated = true;
            }
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return isUpdated;
    }
}
