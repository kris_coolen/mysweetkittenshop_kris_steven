package be.skcorps.hellokitten.repository.contact;

import be.skcorps.hellokitten.model.Contact;
import be.skcorps.hellokitten.repository.JPARepository;

public interface ContactDAO extends JPARepository<Contact,Integer> {

    //eventueel extra dao functies specief voor de klasse Contact
}
