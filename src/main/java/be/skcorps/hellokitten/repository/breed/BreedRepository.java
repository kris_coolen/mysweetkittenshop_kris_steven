package be.skcorps.hellokitten.repository.breed;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.persistence.PersistenceManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

public class BreedRepository implements BreedDAO {
    private EntityManagerFactory emf = null;

    public BreedRepository(String persistenceUnit){
        emf = PersistenceManager.getInstance().getFactory(persistenceUnit);
    }

    public BreedRepository() {
        this("webshop");
    }

    @Override
    public List<Breed> findAll() {
        EntityManager em = null;
        List<Breed> breeds = null;
        try{
            em = emf.createEntityManager();
            TypedQuery<Breed> query = em.createNamedQuery("Breed.findAllBreeds",Breed.class);
            breeds = query.getResultList();
        }catch (Exception exception){
            exception.printStackTrace();

        } finally {
            if (em != null)
                em.close();

        }
        return breeds;
    }

    @Override
    public Breed findByID(Integer id) {
        EntityManager em = null;
        Breed breed = null;
        try{
            em = emf.createEntityManager();
            TypedQuery<Breed> query = em.createQuery("select breed from Breed as breed where breed.id="+id,Breed.class);
            breed = query.getSingleResult();

        }catch(Exception exception){
            exception.printStackTrace();
        }
        finally{
            if(em!=null) em.close();
        }
        return breed;
    }

    @Override
    public Breed save(Breed breed) {
        EntityManager em = null;
        EntityTransaction tx = null;
        try{
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(breed);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return breed;
    }

    @Override
    public boolean delete(Breed breed)
    {
        EntityManager em=null;
        EntityTransaction tx = null;
        boolean isDeleted = false;
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Breed breedDB = em.find(Breed.class,breed.getId());
            tx.commit();
            if (breed != null) {
                tx.begin();
                em.remove(breedDB);
                tx.commit();
                isDeleted = true;
            }
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return isDeleted;
    }

    @Override
    public boolean update(Breed breed)
    {
        EntityManager em = null;
        EntityTransaction tx = null;
        boolean isUpdated =false;
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Breed breedDB = em.find(Breed.class, breed.getId());
            tx.commit();
            if (breedDB != null){
                tx.begin();
                breedDB.setPrice(breed.getPrice());
                breedDB.setStock(breed.getStock());
                tx.commit();
                isUpdated = true;
            }
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return isUpdated;
    }
}
