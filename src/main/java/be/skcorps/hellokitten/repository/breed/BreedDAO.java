package be.skcorps.hellokitten.repository.breed;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.hellokitten.repository.JPARepository;

public interface BreedDAO extends JPARepository<Breed,Integer> {

    //eventueel extra dao functies specief voor de klasse Breed
}
