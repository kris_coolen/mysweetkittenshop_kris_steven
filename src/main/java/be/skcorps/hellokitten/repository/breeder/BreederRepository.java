package be.skcorps.hellokitten.repository.breeder;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.hellokitten.model.Breeder;
import be.skcorps.hellokitten.model.Contact;
import be.skcorps.persistence.PersistenceManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

public class BreederRepository implements BreederDAO {
    private EntityManagerFactory emf = null;

    public BreederRepository(){
        this("webshop");
    }
    public BreederRepository(String persistenceUnit){
        emf = PersistenceManager.getInstance().getFactory(persistenceUnit);
    }

    @Override
    public List<Breeder> findAll() {
        EntityManager em = null;
        List<Breeder> breeders = null;
        try{
            em = emf.createEntityManager();
            TypedQuery<Breeder> query = em.createNamedQuery("Breeder.findAllBreeders",Breeder.class);
            breeders = query.getResultList();
        }catch (Exception exception){
            exception.printStackTrace();

        } finally {
            if (em != null)
                em.close();

        }
        return breeders;
    }

    @Override
    public Breeder findByID(Integer id) {
        EntityManager em = null;
        Breeder breeder = null;
        try{
            em = emf.createEntityManager();
            TypedQuery<Breeder> query =
                    em.createQuery("select breeder from Breeder as breeder where breeder.id="+id,Breeder.class);
            breeder = query.getSingleResult();

        }catch(Exception exception){
            exception.printStackTrace();
        }
        finally{
            if(em!=null) em.close();
        }
        return breeder;
    }

    @Override
    public Breeder save(Breeder breeder) {
        EntityManager em = null;
        EntityTransaction tx = null;
        try{
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            em.persist(breeder);
            tx.commit();
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return breeder;
    }

    @Override
    public boolean delete(Breeder breeder)
    {
        EntityManager em=null;
        EntityTransaction tx = null;
        boolean isDeleted = false;
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Breeder breederDB = em.find(Breeder.class,breeder.getId());
            tx.commit();
            if (breeder != null) {
                tx.begin();
                em.remove(breederDB);
                tx.commit();
                isDeleted = true;
            }
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return isDeleted;
    }

    @Override
    public boolean update(Breeder breeder) {

        EntityManager em = null;
        EntityTransaction tx = null;
        boolean isUpdated =false;
        try {
            em = emf.createEntityManager();
            tx = em.getTransaction();
            tx.begin();
            Breeder breederDB = em.find(Breeder.class, breeder.getId());
            tx.commit();
            if (breederDB != null){
                tx.begin();
                breederDB.setName(breeder.getName());
                breederDB.setContact(breeder.getContact());
                breederDB.setBreeds(breeder.getBreeds());
                tx.commit();
                isUpdated = true;
            }
        }catch (Exception e){
            tx.rollback();
            e.printStackTrace();
        }
        finally {
            if(em!=null) em.close();
        }
        return isUpdated;
    }
}
