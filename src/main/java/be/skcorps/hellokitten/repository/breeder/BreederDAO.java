package be.skcorps.hellokitten.repository.breeder;

import be.skcorps.hellokitten.model.Breeder;
import be.skcorps.hellokitten.repository.JPARepository;

public interface BreederDAO extends JPARepository<Breeder,Integer> {
    //eventueel extra dao functies specief voor de klasse Breeder
}
