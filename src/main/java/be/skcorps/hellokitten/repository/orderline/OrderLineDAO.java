package be.skcorps.hellokitten.repository.orderline;

import be.skcorps.hellokitten.model.OrderLine;
import be.skcorps.hellokitten.repository.JPARepository;

public interface OrderLineDAO extends JPARepository<OrderLine,Integer> {
}
