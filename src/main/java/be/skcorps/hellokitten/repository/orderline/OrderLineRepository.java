package be.skcorps.hellokitten.repository.orderline;

import be.skcorps.hellokitten.model.OrderLine;

import java.util.List;

public class OrderLineRepository implements OrderLineDAO {
    @Override
    public List<OrderLine> findAll() {
        return null;
    }

    @Override
    public OrderLine findByID(Integer integer) {
        return null;
    }

    @Override
    public OrderLine save(OrderLine entity) {
        return null;
    }

    @Override
    public boolean delete(OrderLine entity) {
        return false;
    }

    @Override
    public boolean update(OrderLine entity) {
        return false;
    }
}
