package be.skcorps.hellokitten.repository;

import be.skcorps.hellokitten.model.Breed;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InitialDataBase {

    public static String[] firstNames={"Karolien","Freya","Karel","Suzzy","Levi","Fleur","Kelly","Sonja","Sandra","Anke",
            "Lieve","Katia","Wiese","Catharina"};

    public static String[] lastNames={"De Smet","Lievens","de Grote","Lalijo","Helders","Potters","Verlinde","Verliefde",
            "Gezelle","Van Hoven", "De Schat","Ligatino","Berlind","Bruyntjes"};

    public static String[] streets={"FraiseLaan","Van Loo Straat","Burggrachtdreef","Vleeshuisstraat","Vlindertuinstraat",
            "Chrysanten straat","Gentseweg","Cupidolaan","Franksestraat","Hofseweg",
            "Kasseiweg","Heirbaan","Vleetseweg","Mechelsevest"};

    public static String[] numbers={"22","10","1A","174","42","500","900B","13","7","94", "65","233","23B","90"};

    public static String[] zipCodes={null,null,null,null,null,null,null,null,"2345AB",null,null,null,null,null};

    public static String[] cities ={"Oostende","Aalter","Gent","Merelbeke","Gent","Eeklo","Merelbeke","Gent","Leiden",
            "Merelbeke","Gent","Antwerpen","Vleeteren","Oostende"};

    public static String[] emails={"Karolien.de.smet@gmail.com","Freya.poessie@hotmail.com","karelvagrote@hotmail.com",
            "Suzzy.wuzzy@me.com","Levi@multimedi.be","fleur@msn.be","kellieke@versegroenten.be",
            "info@blackmystic.be","info@sequoaiahoeve.nl","info@ankeshof.be","Lieve.poezies@gmail.com",
            "info@katias.palazo.be","Wizzy.info@kitties.be","info@groot-kathof.be"};

    public static String[] telephones={"+32473645399","+32492332233","+32487756473","+32499934843","+32491234563",
            "+32472323645","+32610283736","+32475101044","+32688347273","+32484328432",
            "+32490101010","+32483618361","+32491231239","+32472793542"};

    public static List<String> breederNames = new ArrayList<>(
            Arrays.asList("Black Mystic Legend","Sequoia Hoeve","Ankeshof","Lieve Poezies",
                    "Katia's Palazo","Kitty Wizzy","Groot Kathof"));
    public static List<Integer> contactIdList = new ArrayList<>(Arrays.asList(8,9,10,11,12,13,14));
    public static ArrayList<ArrayList<Integer>> breedIdsOf = new ArrayList<>(breederNames.size());

    public static List<Breed> breedList;

    public static void init(){
        initBreeds();
        initBreedIdsOf();
    }

    public static void initBreedIdsOf(){
        breedIdsOf.add(new ArrayList<>(Arrays.asList(1)));
        breedIdsOf.add(new ArrayList<>(Arrays.asList(3,5)));
        breedIdsOf.add(new ArrayList<>(Arrays.asList(8)));
        breedIdsOf.add(new ArrayList<>(Arrays.asList(11,13)));
        breedIdsOf.add(new ArrayList<>(Arrays.asList(6,15)));
        breedIdsOf.add(new ArrayList<>(Arrays.asList(9,17)));
        breedIdsOf.add(new ArrayList<>(Arrays.asList(2,4,7,10,12,14,16,18,19)));
    }

    public static void initBreeds(){
        Breed breed1 = new Breed("Heilige Birmaan",1400.0,1); //con 8
        Breed breed2 = new Breed("Heilige Birmaan",1245.0,5); //con 14
        Breed breed3 = new Breed("Britse Korthaar",900.0,2);//con 9
        Breed breed4 = new Breed("Britse Korthaar",750.0,3); //con 14
        Breed breed5 = new Breed("Europese Korthaar",500.0,3);//con 9
        Breed breed6 = new Breed("Europese Korthaar",400.0,4);//con 12
        Breed breed7 = new Breed("Europese Korthaar",350.0,5);//con 14
        Breed breed8 = new Breed("Naakt",1100.0,4);//con 10
        Breed breed9 = new Breed("Naakt",1000.0,5);//con 13
        Breed breed10 = new Breed("Naakt",900.0,6);//con 14
        Breed breed11= new Breed("Main Coon",1000.0,2); //con11
        Breed breed12= new Breed("Main Coon",950.0,3); //con14
        Breed breed13= new Breed("Perzische",475.0,4); //con11
        Breed breed14= new Breed("Perzische",450.0,6); //con14
        Breed breed15 = new Breed("Noorse Bos",685.0,7); //con12
        Breed breed16 = new Breed("Noorse Bos",625.0,3); //con14
        Breed breed17=new Breed("Siamees",850.0,1); //con13
        Breed breed18=new Breed("Siamees",650.0,4); //con14
        Breed breed19=new Breed("Bengaal",1200.0,3); //con14

        breedList= new ArrayList<>(Arrays.asList(breed1,breed2,breed3,breed4,breed5,breed6,
                breed7,breed8,breed9,breed10,breed11,breed12,breed13,breed14,breed15,breed16,breed17,
                breed18,breed19,breed19));
    }
}
