package be.skcorps.hellokitten.repository.order;

import be.skcorps.hellokitten.model.Order;
import be.skcorps.hellokitten.repository.JPARepository;

public interface OrderDAO extends JPARepository<Order,Integer> {
}
