package be.skcorps.hellokitten.repository.order;

import be.skcorps.hellokitten.model.Order;

import java.util.List;

public class OrderRepository implements OrderDAO {
    @Override
    public List<Order> findAll() {
        return null;
    }

    @Override
    public Order findByID(Integer integer) {
        return null;
    }

    @Override
    public Order save(Order entity) {
        return null;
    }

    @Override
    public boolean delete(Order entity) {
        return false;
    }

    @Override
    public boolean update(Order entity) {
        return false;
    }
}
