package be.skcorps.hellokitten.repository;

import java.util.List;

public interface JPARepository<T,ID> {

    public List<T> findAll();

    public T findByID(ID id);

    public T save(T entity);

    public boolean delete(T entity);

    public boolean update(T entity);


    }
