package be.skcorps.hellokitten.service;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.hellokitten.repository.breed.BreedDAO;

import java.util.List;

public class BreedService {

    private BreedDAO breedDao;

    public BreedService(BreedDAO breedDao){
        this.breedDao=breedDao;
    }

    public List<Breed> findAllBreeds(){
        return breedDao.findAll();
    }

    public Breed findBreedById(Integer id){
        return breedDao.findByID(id);
    }

    public Breed saveBreed(Breed breed){
        return breedDao.save(breed);
    }

    public boolean removeBreed(Breed breed){ return breedDao.delete(breed);}

    public boolean updateBreed(Breed breed){return breedDao.update(breed);}


}
