package be.skcorps.hellokitten.service;

import be.skcorps.hellokitten.model.Breed;
import be.skcorps.hellokitten.model.Breeder;
import be.skcorps.hellokitten.repository.breeder.BreederDAO;

import java.util.List;

public class BreederService {

    private BreederDAO breederDAO;

    public BreederService(BreederDAO breederDAO){
        this.breederDAO=breederDAO;
    }

    public List<Breeder> findAllBreeders(){
        return breederDAO.findAll();
    }

    public Breeder findBreederById(Integer id){
        return breederDAO.findByID(id);
    }

    public Breeder saveBreeder(Breeder breeder){
        return breederDAO.save(breeder);
    }

    public boolean removeBreeder(Breeder breeder){ return breederDAO.delete(breeder);}

    public boolean updateBreeder(Breeder breeder){return breederDAO.update(breeder);}
}
