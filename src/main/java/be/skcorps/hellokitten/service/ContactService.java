package be.skcorps.hellokitten.service;

import be.skcorps.hellokitten.model.Contact;
import be.skcorps.hellokitten.repository.contact.ContactDAO;

import java.util.List;

public class ContactService {

    private ContactDAO contactDAO;

    public ContactService(ContactDAO contactDAO){
        this.contactDAO = contactDAO;
    }


    public List<Contact> findAllContacts(){
        return contactDAO.findAll();
    }

    public Contact findContactById(Integer id){
        return contactDAO.findByID(id);
    }

    public Contact saveContact(Contact contact){
        return contactDAO.save(contact);
    }

    public boolean updateContact(Contact contact){
        return contactDAO.update(contact);
    }

    public boolean removeContact(Contact contact){
        return contactDAO.delete(contact);
    }
}
